import EasyAccess, { defaultMutations } from "vuex-easy-access";

export const state = () => ({
  datas: [],
  data_detail: null,

  loading: false,
  loading_detail: true
});

export const mutations = { ...defaultMutations(state()) };

export const plugins = [EasyAccess()];

export const actions = {
  loadData({ dispatch }) {
    dispatch("set/loading", true);

    return this.$axios
      .get(`https://5f50ca542b5a260016e8bfb0.mockapi.io/api/v1/movies`)
      .then(res => {
        dispatch("set/loading", false);
        dispatch("set/datas", res.data);
        return true;
      })
      .catch(err => {
        dispatch("set/loading", false);
        return false;
      })
      .finally(() => {});
  },
  loadDataDetail({ dispatch }, id) {
    dispatch("set/loading_detail", true);

    return this.$axios
      .get(`https://5f50ca542b5a260016e8bfb0.mockapi.io/api/v1/movies/${id}`)
      .then(async res => {
        await dispatch("set/data_detail", res.data);
        dispatch("set/loading_detail", false);
        return true;
      })
      .catch(err => {
        dispatch("set/loading_detail", false);
        return false;
      })
      .finally(() => {});
  }
};
